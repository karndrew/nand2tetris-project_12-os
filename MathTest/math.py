NUMBER_SIZE = 16
twoToThe = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32767]

def multiply(x, y):
    bits = '['

    sum = 0
    i = 0
    shiftedX = x

#if x >= 0 and y >= 0:
    while i < NUMBER_SIZE:
        if bit(y, i) == 1:
            sum = sum + shiftedX
            bits = bits + '1,'
        else:
            bits = bits + '0,'
        i = i + 1

        shiftedX = shiftedX + shiftedX

    bits = bits[0:len(bits) - 1] + ']'

    print(bits)
    return sum

def bit(x, i):
    index = NUMBER_SIZE - 1
    sum = 0
    has_bit = True

    while index > i - 1:
        currentSum = sum + twoToThe[index]

        if currentSum <= x:
            sum = currentSum
            has_bit = True
        else:
            has_bit = False

        index = index - 1

    return has_bit


def main():
    print(str(multiply(6, -30)))
    #print(str(bit(6, 0)))

if __name__ == "__main__":
    main()
