// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/12/String.jack

/**
 * Represents character strings. In addition for constructing and disposing
 * strings, the class features methods for getting and setting individual
 * characters of the string, for erasing the string's last character,
 * for appending a character to the string's end, and more typical
 * string-oriented operations.
 */
class String {

    field Array string;
    field int length, maxLen;

    /** constructs a new empty string with a maximum length of maxLength
     *  and initial length of 0. */
    constructor String new(int maxLength) {
      if (maxLength > 0) {
            let string = Array.new(maxLength);
        } else {
            let string = Memory.alloc(1);
        }

      let length = 0;
      let maxLen = maxLength;

      return this;
    }

    /** Disposes this string. */
    method void dispose() {
      do string.dispose();
      do Memory.deAlloc(this);
      return;
    }

    /** Returns the current length of this string. */
    method int length() {
      return length;
    }

    /** Returns the character at the j-th location of this string. */
    method char charAt(int j) {
      if ( j < length ){
        return string[j];
      }
      return 0;
    }

    /** Sets the character at the j-th location of this string to c. */
    method void setCharAt(int j, char c) {
      if ( j < length ){
        let string[j] = c;
        return;
      }
      return;
    }

    /** Appends c to this string's end and returns this string. */
    method String appendChar(char c) {
       if (length < maxLen) {
        let string[length] = c;
        let length = length + 1;
      }

      return this;
    }

    /** Erases the last character from this string. */
    method void eraseLastChar() {
      if (length > 0){
        let length = length - 1;
        let string[length] = 0;
      }

      return;
    }

    /** Returns the integer value of this string,
     *  until a non-digit character is detected. */
    method int intValue() {
      var int value, digit;
      var int character;
      var boolean negInt;

      let character = 0;
      let value = 0;
      let negInt = false;

        if (string[character] = 45) {
            let negInt = true;
            let character = character + 1;
        }


      while ( (character < length) & (string[character] > 47) & (string[character] < 58) ) {
        let digit = string[character] - 48;
        let value = value * 10;
        let value = value + digit;

        let character = character + 1;
      }

      if (negInt) {
           return -value;
       } else {
           return value;
       }

    }

    /** Sets this string to hold a representation of the given value. */
    method void setInt(int val) {
      var int digit;
        var int c;
        var String res;

        let res = String.new(5);
        let length = 0;

        if (val < 0) {
            do appendChar(45);
            let val = -val;
        }

        while (val > 0) {
            let digit = val - ((val/10)*10);
            let c = digit + 48;
            do res.appendChar(c);
            let val = val/10;
        }

        let c = 0;

        while ( c < res.length() ) {
            do appendChar(res.charAt(res.length() - c - 1));
            let c = c + 1;
        }
        return;
    }

    /** Returns the new line character. */
    function char newLine() {
      return 128;
    }

    /** Returns the backspace character. */
    function char backSpace() {
      return 129;
    }

    /** Returns the double quote (") character. */
    function char doubleQuote() {
      return 34;
    }
}
